﻿--1. What are the sales, product costs, profit, number of orders & quantity ordered for internet sales by product category and ranked by sales? 

select 
rank() over   
    (order by sum(a.LineTotal) desc) as Rank,  
e.Name as 'Name',
sum(a.OrderQty) as 'TotalQuantity',
count( distinct a.SalesOrderID) as 'TotalNumberofOrders',
sum(a.LineTotal) as 'TotalSales',
sum(c.StandardCost) as 'ProductCost',
sum(a.LineTotal - (c.StandardCost-a.UnitPriceDiscount) ) as 'Profit'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Production].[Product] as c on a.ProductID = c.ProductID
join [Production].[ProductSubcategory] as d on c.ProductSubcategoryID = d.ProductSubcategoryID
join [Production].[ProductCategory] as e on d.ProductCategoryID = e.ProductCategoryID
where b.OnlineOrderFlag = 1 
group by e.Name 

--2. What are the sales, product costs, profit, number of orders & quantity ordered for reseller sales by product category and ranked by sales? 
	
	select 
rank() over   
    (order by sum(a.LineTotal) desc) as rank,  
e.Name as 'Name',
sum(a.OrderQty) as 'TotalQuantity',
count( distinct a.SalesOrderID) as 'TotalNumberofOrders',
sum(a.LineTotal) as 'TotalSales',
sum(c.StandardCost) as 'ProductCost',
sum(a.LineTotal - (c.StandardCost-a.UnitPriceDiscount) ) as 'Profit'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Production].[Product] as c on a.ProductID = c.ProductID
join [Production].[ProductSubcategory] as d on c.ProductSubcategoryID = d.ProductSubcategoryID
join [Production].[ProductCategory] as e on d.ProductCategoryID = e.ProductCategoryID
where b.OnlineOrderFlag = 0 
group by e.Name

--3. What are the sales, product costs, profit, number of orders & quantity ordered for both internet & reseller sales by product category and ranked by sales? 

select 
rank() over   
    (order by sum(a.LineTotal) desc) as Rank,  
e.Name as 'Name',
sum(a.OrderQty) as 'TotalQuantity',
count( distinct a.SalesOrderID) as 'TotalNumberofOrders',
sum(a.LineTotal) as 'TotalSales',
sum(c.StandardCost) as 'ProductCost',
sum(a.LineTotal - (c.StandardCost-a.UnitPriceDiscount) ) as 'Profit'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Production].[Product] as c on a.ProductID = c.ProductID
join [Production].[ProductSubcategory] as d on c.ProductSubcategoryID = d.ProductSubcategoryID
join [Production].[ProductCategory] as e on d.ProductCategoryID = e.ProductCategoryID
group by e.Name

--4. What are the sales, product costs, profit, number of orders & quantity ordered for product category Accessories broken-down by Product Hierarchy (Category, Subcategory, Model & Product) for both internet & reseller sales? 
	
	select 
e.Name as 'Product Category',
d.Name as 'Product Subcategory',
f.Name as 'Model',
c.Name as 'Product Name',
sum(a.OrderQty) as 'TotalQuantity',
count( distinct a.SalesOrderID) as 'TotalNumberofOrders',
sum(a.LineTotal) as 'TotalSales'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Production].[Product] as c on a.ProductID = c.ProductID
join [Production].[ProductSubcategory] as d on c.ProductSubcategoryID = d.ProductSubcategoryID
join [Production].[ProductCategory] as e on d.ProductCategoryID = e.ProductCategoryID
join [Production].[ProductModel] as f on c.ProductModelID = f.ProductModelID
where e.Name = 'Accessories'
group by e.Name,d.Name,f.Name,c.Name
order by 'TotalSales' desc

--5. What are the sales, product costs, profit, number of orders & quantity ordered for both internet & reseller sales by country and ranked by sales? 
	
	select 
rank() over   
    (order by sum(a.LineTotal) desc) as Rank,  
f.Name,
c.Name as 'Product Name',
sum(c.StandardCost) as 'ProductCost',
sum(a.LineTotal - c.StandardCost) as 'Profit',
sum(a.OrderQty) as 'TotalQuantity',
count( distinct a.SalesOrderID) as 'TotalNumberofOrders',
sum(a.LineTotal) as 'TotalSales'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Production].[Product] as c on a.ProductID = c.ProductID
join [Person].[Address] as d on b.BillToAddressID = d.AddressID
join [Person].[StateProvince] as e on d.StateProvinceID = e.StateProvinceID
join [Person].[CountryRegion] as f on e.CountryRegionCode = f.CountryRegionCode
group by f.Name,c.Name
order by 'TotalSales' desc

--6. What are the sales, product costs, profit, number of orders & quantity ordered for France by city and ranked by salesfor both internet & reseller sales? 
	select 
rank() over   
    (order by sum(a.LineTotal) desc) as Rank, 
f.Name as CountryName,
d.City as CityName,
sum(a.OrderQty) as 'TotalQuantity',
sum(c.StandardCost) as 'ProductCost',
sum(a.LineTotal - c.StandardCost) as 'Profit',
count( distinct a.SalesOrderID) as 'TotalNumberofOrders',
sum(a.LineTotal) as 'TotalSales'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Production].[Product] as c on a.ProductID = c.ProductID
join [Person].[Address] as d on b.BillToAddressID = d.AddressID
join [Person].[StateProvince] as e on d.StateProvinceID = e.StateProvinceID
join [Person].[CountryRegion] as f on e.CountryRegionCode = f.CountryRegionCode
where f.Name = 'France'
group by f.Name,d.City
order by 'TotalSales' desc

--7. What are the top ten resellers by reseller hierarchy (business type, reseller name) ranked by sales? 

select top 10 sum(a.SubTotal) as 'Sales',
       b.Name as 'Reseller Name',
	   c.BusinessType
from Sales.SalesOrderHeader a
join Sales.Store b
on a.SalesPersonID = b.SalesPersonID
join Sales.vStoreWithDemographics c
on b.BusinessEntityID = c.BusinessEntityID
group by c.BusinessType , b.Name
order by Sales desc

--8. What are the top ten (internet) customers ranked by sales? 

select top 10 Rank() over(order by TotalSales desc) as Rank , *
from (
select concat(e.LastName , ', ', e.FirstName) as FullName, 
sum(b.SubTotal) as 'TotalSales'
from [Sales].[SalesOrderHeader] as b 
join [Sales].[Customer] as d on b.CustomerID = d.CustomerID
join [Person].[Person]  as e on d.PersonID = e.BusinessEntityID
where b.OnlineOrderFlag = 1
group by concat(e.LastName , ', ', e.FirstName)
)R
order by Rank

--9. What are the sales, product costs, profit, number of orders & quantity ordered by Customer Occupation? 

select b.Occupation,
sum(d.LineTotal) as 'TotalSales',
count(distinct c.SalesOrderID) as 'TotalOrderCount',
sum(d.LineTotal - e.StandardCost) as 'TotalProfit',
sum(d.OrderQty) as 'TotalQuantity',
sum(e.StandardCost) as 'ProductCost'
from [Sales].[Customer] as a
join [Sales].[vPersonDemographics] as b on a.PersonID = b.BusinessEntityID
join [Sales].[SalesOrderHeader] as c on a.CustomerID = c.CustomerID
join [Sales].[SalesOrderDetail] as d on c.SalesOrderID = d.SalesOrderID
join [Production].[Product] as e on d.ProductID = e.ProductID
where c.OnlineOrderFlag = 1
group by b.Occupation
order by TotalSales desc

--10. What are the ranked sales of the sales people (employees)? 

	Select top 10 Rank() over(order by Total_Sales desc) as Rank , *
from 
(
select  sum(SubTotal) as 'Total_Sales'  ,CONCAT(d.LastName , ', ' , d.FirstName) as 'FullName' 
from Sales.SalesOrderHeader a
join Sales.SalesPerson b
on b.BusinessEntityID = a.SalesPersonID
join HumanResources.Employee c
on b.BusinessEntityID = c.BusinessEntityID
join Person.Person d
on b.BusinessEntityID = d.BusinessEntityID
group by CONCAT(d.LastName , ', ' , d.FirstName)
) z
order by Rank

--11. What are the sales, discount amounts (promotion discounts), profit and promotion % of sales for Reseller Sales by Promotion Hierarchy (Category, Type & Name) – sorted descending by sales.? 

	select c.Category,
c.Type,
c.Description,
sum(a.LineTotal) as 'TotalSales',
sum(a.UnitPrice * a.UnitPriceDiscount) as 'DiscountAmount',
sum(a.LineTotal - d.StandardCost) as 'Profit',
(sum(a.UnitPrice * a.UnitPriceDiscount) / sum(a.LineTotal) )* 100 as 'Promotion Percent'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Sales].[SpecialOffer] as c on a.SpecialOfferID = c.SpecialOfferID
join [Production].[Product] as d on a.ProductID = d.ProductID
group by c.Category,c.Type,c.Description
order by TotalSales desc

--12. What are the sales, product costs, profit, number of orders & quantity ordered by Sales Territory Hierarchy (Group, Country, region) and ranked by sales for both internet & reseller sales? 

	select 
rank() over   
    (order by sum(a.LineTotal) desc) as Rank, 
e.Name as 'CountryName',
d."Group" as 'GroupName',
d.Name as RegionName,
sum(c.StandardCost) as 'ProductCost',
sum(a.LineTotal - c.StandardCost) as 'TotalProfit',
sum(a.OrderQty) as 'TotalQuantity',
count( distinct a.SalesOrderID) as 'TotalNumberofOrders',
sum(a.LineTotal) as 'TotalSales'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Production].[Product] as c on a.ProductID = c.ProductID
join [Sales].[SalesTerritory] as d on b.TerritoryID = d.TerritoryID
join [Person].[CountryRegion] as e on d.CountryRegionCode = e.CountryRegionCode
group by d."Group",e.Name,d.Name
order by 'TotalSales' desc

--13. What are the sales by year by sales channels (internet, reseller & total)? 

select 
case when (b.OnlineOrderFlag = 0) then 'Reseller' when (b.OnlineOrderFlag = 1) then 'InternetSales' end as Channel,
year(b.OrderDate) as 'Year',
sum(a.LineTotal) as 'TotalSales'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Production].[Product] as c on a.ProductID = c.ProductID
join [Production].[ProductSubcategory] as d on c.ProductSubcategoryID = d.ProductSubcategoryID
join [Production].[ProductCategory] as e on d.ProductCategoryID = e.ProductCategoryID
group by year(b.OrderDate),b.OnlineOrderFlag
union all
select 'Total',
year(b.OrderDate) as 'Year',
sum(a.LineTotal) as 'TotalSales'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
join [Production].[Product] as c on a.ProductID = c.ProductID
join [Production].[ProductSubcategory] as d on c.ProductSubcategoryID = d.ProductSubcategoryID
join [Production].[ProductCategory] as e on d.ProductCategoryID = e.ProductCategoryID
group by year(b.OrderDate)

--14. What are the total sales by month (& year)? 

select 
year(b.OrderDate) as Year,
month(b.OrderDate) as Month,
sum(a.LineTotal) as 'TotalSales'
from [Sales].[SalesOrderDetail] as a
join [Sales].[SalesOrderHeader] as b on a.SalesOrderID = b.SalesOrderID
group by year(b.OrderDate),month(b.OrderDate)
order by 'TotalSales' desc

--15. Please explain (briefly) the differences between SQL queries used to answer the same questions between AdventureWorksDW2017 & AdventureWorks2017
--•	AdventureWorksDW2017 has more of a segregated data so it is easy to iterate through all the tables to get the required results.
--•	AdventureWorksDW2017 is a single schema db so it has direct relations with all the related tables.
--•	AdventureWorksDW2017 is a better data source for analysis of data
--•	In DW there were different tables for internet and reseller whereas in OLTP we had to bifurcate on the basis of a flag (OnlineOrderFlag).
--•	We had Orderdatekey in DW which made it easy to get the year and month while quering for the same.
--•	It was easier to get the Total sales for all the available seller regardless of platform in OLTP as all the data was in one table. In DW union had to be done for Internet and Reseller.
--•	Getting occupation result was easier in DW as we had direct column with EnglishOccupation in the Table.
--•	Total sales by year was easier to get in the OLTP as in DW there were different tables with internet and reseller sales which resulted in multiple unions making it difficult to use ‘rollup’ as well.
--•	Getting Territory details as country, city, region was easier in DW as it was segregated in one table whereas OLTP had all the parameters in different table.

