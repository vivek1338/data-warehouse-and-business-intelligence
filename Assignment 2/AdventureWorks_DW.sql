﻿--1. What are the sales, product costs, profit, number of orders & quantity ordered for internet sales by product category and ranked by sales? 

select
rank() over   
    (order by sum(a.SalesAmount) desc) as Rank,  
d.EnglishProductCategoryName as Name,
sum(a.OrderQuantity) as TotalQuantity,
count(distinct a.SalesOrderNumber) as TotalNumberOfOrders,
sum(a.SalesAmount) as TotalSales,
sum(a.TotalProductCost) as TotalProductCost,
sum(a.SalesAmount - a.TotalProductCost) as TotalProfit
from FactInternetSales as a
join DimProduct as b on a.ProductKey = b.ProductKey
join DimProductSubcategory as c on b.ProductSubcategoryKey = c.ProductSubcategoryKey
join DimProductCategory as d on c.ProductCategoryKey = d.ProductCategoryKey
group by d.EnglishProductCategoryName
order by TotalSales desc
 
--2. What are the sales, product costs, profit, number of orders & quantity ordered for reseller sales by product category and ranked by sales? 
	select
rank() over   
    (order by sum(a.SalesAmount) desc) as Rank,  
d.EnglishProductCategoryName as Name,
sum(a.OrderQuantity) as TotalQuantity,
count(distinct a.SalesOrderNumber) as TotalNumberOfOrders,
sum(a.SalesAmount) as TotalSales,
sum(a.TotalProductCost) as TotalProductCost,
sum(a.SalesAmount - a.TotalProductCost) as TotalProfit
from FactResellerSales as a
join DimProduct as b on a.ProductKey = b.ProductKey
join DimProductSubcategory as c on b.ProductSubcategoryKey = c.ProductSubcategoryKey
join DimProductCategory as d on c.ProductCategoryKey = d.ProductCategoryKey
group by d.EnglishProductCategoryName
order by TotalSales desc
 
--3. What are the sales, product costs, profit, number of orders & quantity ordered for both internet & reseller sales by product category and ranked by sales? 

	select Rank() over(order by Sales desc) as Rank , *
from 
(
select ProductCategory, 
sum(K.Quantity) as 'Quantity',
sum(K.NumberofOrders) as 'NumberofOrders' ,
sum(K.Sales) as 'Sales' , 
sum (K.ProductCost) as 'ProductCost',
sum(K.Profit)as 'Profit'  from
(
select sum (a.SalesAmount) as 'Sales',
       sum(a.TotalProductCost) as 'ProductCost' ,
	  (sum(a.SalesAmount) - sum(a.TotalProductCost) ) as 'Profit',
	  count(distinct a.SalesOrderNumber) as 'NumberofOrders',
	  sum(a.OrderQuantity) as 'Quantity',
	  d.EnglishProductCategoryName as 'ProductCategory'
from FactInternetSales a
join DimProduct b
on a.ProductKey = b.ProductKey
join DimProductSubcategory c
on b.ProductSubcategoryKey = c.ProductSubcategoryKey
join DimProductCategory d
on d.ProductCategoryKey = c.ProductCategoryKey
group by d.EnglishProductCategoryName


UNION

select sum (a.SalesAmount) as 'Sales',
       sum(a.TotalProductCost) as 'ProductCost' ,
	  (sum(a.SalesAmount) - sum(a.TotalProductCost) ) as 'Profit',
	  count(distinct a.SalesOrderNumber) as 'NumberofOrders',
	  sum(a.OrderQuantity) as 'Quantity',
	  d.EnglishProductCategoryName as 'ProductCategory'
from FactResellerSales a
join DimProduct b
on a.ProductKey = b.ProductKey
join DimProductSubcategory c
on b.ProductSubcategoryKey = c.ProductSubcategoryKey
join DimProductCategory d
on d.ProductCategoryKey = c.ProductCategoryKey
group by d.EnglishProductCategoryName )
K
group by [ProductCategory]
)R
order by Rank
 
--4. What are the sales, product costs, profit, number of orders & quantity ordered for product category Accessories broken-down by Product Hierarchy (Category, Subcategory, Model & Product) for both internet & reseller sales? 

	select sum(R.Sales) as 'Sales' , sum (R.ProductCost) as 'ProductCost', sum(R.NumberofOrders) as 'NumberofOrders' ,
       sum(R.Profit)as 'Profit', sum(R.Quantity) as 'Quantity' , ProductCategory , SubCategory , ProductName , ModelName
	   
	   from
(
select sum (a.SalesAmount) as 'Sales',
       sum(a.TotalProductCost) as 'ProductCost' ,
	  (sum(a.SalesAmount) - sum(a.TotalProductCost) ) as 'Profit',
	  count(distinct a.SalesOrderNumber) as 'NumberofOrders',
	  sum(a.OrderQuantity) as 'Quantity',
	  d.EnglishProductCategoryName as 'ProductCategory',
	  c.EnglishProductSubcategoryName as 'SubCategory' ,
	  b.EnglishProductName as 'ProductName' ,
	  b.ModelName          as 'ModelName'
from FactInternetSales a
join DimProduct b
on a.ProductKey = b.ProductKey
join DimProductSubcategory c
on b.ProductSubcategoryKey = c.ProductSubcategoryKey
join DimProductCategory d
on d.ProductCategoryKey = c.ProductCategoryKey
where d.EnglishProductCategoryName = 'Accessories'
group by d.EnglishProductCategoryName , c.EnglishProductSubcategoryName , b.ModelName ,b.EnglishProductName

UNION

select sum (a.SalesAmount) as 'Sales',
       sum(a.TotalProductCost) as 'ProductCost' ,
	  (sum(a.SalesAmount) - sum(a.TotalProductCost) ) as 'Profit',
	  count(distinct a.SalesOrderNumber) as 'NumberofOrders',
	  sum(a.OrderQuantity) as 'Quantity',
	  d.EnglishProductCategoryName as 'ProductCategory',
	  c.EnglishProductSubcategoryName as 'SubCategory' ,
	  b.EnglishProductName as 'ProductName' ,
	  b.ModelName          as 'ModelName'
from FactResellerSales a
join DimProduct b
on a.ProductKey = b.ProductKey
join DimProductSubcategory c
on b.ProductSubcategoryKey = c.ProductSubcategoryKey
join DimProductCategory d
on d.ProductCategoryKey = c.ProductCategoryKey
where d.EnglishProductCategoryName = 'Accessories'
group by d.EnglishProductCategoryName , c.EnglishProductSubcategoryName , b.ModelName ,b.EnglishProductName
) R
group by  ProductCategory , SubCategory , ProductName , ModelName
order by Sales desc
 
--5. What are the sales, product costs, profit, number of orders & quantity ordered for both internet & reseller sales by country and ranked by sales? 

select Rank() over(order by Sales desc) as Rank , *
from 
(
select sum(R.Sales) as 'Sales' , 
       sum (R.ProductCost) as 'ProductCost', 
	   sum(R.NumberofOrders) as 'NumberofOrders' ,
       sum(R.Profit)as 'Profit', 
	   sum(R.Quantity) as 'Quantity' , 
	   Country 
from
(
select sum (a.SalesAmount) as 'Sales', 
       sum(a.TotalProductCost) as 'ProductCost' , 
	   (sum(a.SalesAmount) - sum(a.TotalProductCost) ) as 'Profit',
	   count(distinct a.SalesOrderNumber) as 'NumberofOrders' , 
	   sum(a.OrderQuantity) as 'Quantity',
	   b.SalesTerritoryCountry as 'Country'
from FactInternetSales a
join DimSalesTerritory b
on b.SalesTerritoryKey = a.SalesTerritoryKey
group by b.SalesTerritoryCountry

UNION

select sum (a.SalesAmount) as 'Sales',
       sum(a.TotalProductCost) as 'ProductCost' ,
	  (sum(a.SalesAmount) - sum(a.TotalProductCost) ) as 'Profit',
	  count(distinct a.SalesOrderNumber) as 'NumberofOrders',
	  sum(a.OrderQuantity) as 'Quantity',
	  b.SalesTerritoryCountry as 'Country'
from FactResellerSales a
join DimSalesTerritory b
on b.SalesTerritoryKey = a.SalesTerritoryKey
group by b.SalesTerritoryCountry
 )
R
group by Country )
K
order by Rank
 
--6. What are the sales, product costs, profit, number of orders & quantity ordered for France by city and ranked by sales for both internet & reseller sales? 

	select rank() over   
    (order by sum(R.TotalSales) desc) as Rank,R.City as 'City',R.Country as 'Country',
sum(R.TotalSales) as 'TotalSales',
sum(R.ProductCost) as 'ProductCost',
sum(R.Profit) as 'Profit',
sum(R.Quantity) as 'Quantity',
sum(R.OrderNumbers) as 'OrderNumbers'
from
(
select c.City as City,c.EnglishCountryRegionName as Country,
sum(a.SalesAmount) as 'TotalSales',
sum(a.TotalProductCost) as 'ProductCost',
sum(a.SalesAmount-a.TotalProductCost) as 'Profit',
sum(a.OrderQuantity) as 'Quantity',
count(distinct a.SalesOrderNumber) as 'OrderNumbers'
from 
FactInternetSales as a
join DimCustomer as b on a.CustomerKey = b.CustomerKey
join DimGeography as c on b.GeographyKey = c.GeographyKey
where c.EnglishCountryRegionName = 'France'
group by c.City,c.EnglishCountryRegionName

union 

select c.City,c.EnglishCountryRegionName,
sum(a.SalesAmount) as 'TotalSales',
sum(a.TotalProductCost) as 'ProductCost',
sum(a.SalesAmount-a.TotalProductCost) as 'Profit',
sum(a.OrderQuantity) as 'Quantity',
count(distinct a.SalesOrderNumber) as 'OrderNumbers'
from 
FactResellerSales as a
join DimReseller as b on a.ResellerKey = b.ResellerKey
join DimGeography as c on b.GeographyKey = c.GeographyKey
where c.EnglishCountryRegionName = 'France'
group by c.City,c.EnglishCountryRegionName
)R

group by R.City,R.Country
order by TotalSales desc
 
--7. What are the top ten resellers by reseller hierarchy (business type, reseller name) ranked by sales? 

	Select Rank() over(order by Sales desc) as Rank , *
from 
(
select top 10 sum(a.SalesAmount) as 'Sales', b.BusinessType , b.ResellerName from FactResellerSales a
join DimReseller b
on a.ResellerKey = b.ResellerKey
group by b.ResellerName , b.BusinessType )
R
order by Rank
 
--8. What are the top ten (internet) customers ranked by sales? 

	Select top 10 Rank() over(order by TotalSales desc) as Rank , *
from (
select 
concat(c.LastName , ', ',c.FirstName) as FullName, 
sum(a.SalesAmount) as TotalSales from FactInternetSales as a
join [dbo].[DimProduct] as b on a.ProductKey = b.ProductKey
join [dbo].[DimCustomer] as c on c.CustomerKey = a.CustomerKey
group by concat(c.LastName , ', ',c.FirstName))R
order by Rank
 
--9. What are the sales, product costs, profit, number of orders & quantity ordered by Customer Occupation? 

	select sum (a.SalesAmount) as 'Sales',
       sum(a.TotalProductCost) as 'ProductCost' ,
	  (sum(a.SalesAmount) - sum(a.TotalProductCost) ) as 'Profit',
	  count(distinct a.SalesOrderNumber) as 'NumberofOrders',
	  sum(a.OrderQuantity) as 'Quantity',
	  b.EnglishOccupation  as 'Occupation'
from FactInternetSales a
join DimCustomer b
on a.CustomerKey = b.CustomerKey
group by b.EnglishOccupation
order by Sales desc
 
--10. What are the ranked sales of the sales people (employees)? 

Select top 10 Rank() over(order by Sales desc) as Rank , *
from 
(
select sum(a.SalesAmount) as 'Sales',  CONCAT(b.LastName , ' ,' , b.FirstName) as 'EmployeeName' 
from FactResellerSales a
join DimEmployee b
on a.EmployeeKey = b.EmployeeKey
group by CONCAT(b.LastName , ' ,' , b.FirstName)
) R
order by Rank
 
--11. What are the sales, discount amounts (promotion discounts), profit and promotion % of sales for Reseller Sales by Promotion Hierarchy (Category, Type & Name) – sorted descending by sales.?

select sum( a.SalesAmount) as 'Sales',
       Sum(a.DiscountAmount) as 'Discount',
      (sum(a.SalesAmount) - sum(a.TotalProductCost) ) as 'Profit' ,
	  b.EnglishPromotionCategory as 'Category' ,
	  b.EnglishPromotionName as 'Name' ,
	  b.EnglishPromotionType as 'Type',
	  (sum(a.DiscountAmount) / sum(a.SalesAmount) )* 100 as 'Promotion Percent'
from FactResellerSales a
join DimPromotion b
on a.PromotionKey = b.PromotionKey
group by b.EnglishPromotionCategory,  b.EnglishPromotionType , b.EnglishPromotionName
order by Sales desc  
 
--12. What are the sales, product costs, profit, number of orders & quantity ordered by Sales Territory Hierarchy (Group, Country, region) and ranked by sales for both internet & reseller sales? 

Select Rank() over(order by Sales desc) as Rank , *
from 
(
select sum(R.Sales) as 'Sales' , 
       sum (R.ProductCost) as 'ProductCost', 
	   sum(R.NumberofOrders) as 'NumberofOrders' ,
       sum(R.Profit)as 'Profit', 
	   sum(R.Quantity) as 'Quantity' , 
	   R.GroupName ,
 R.Country ,
R.Region 
from
(
select  sum(a.SalesAmount) as 'Sales',
		sum(a.TotalProductCost) as 'ProductCost',
		sum(a.SalesAmount - a.TotalProductCost) as 'Profit',
		sum(a.OrderQuantity) as 'Quantity',
		count(distinct a.SalesOrderNumber) as 'NumberofOrders',
		b.SalesTerritoryGroup as 'GroupName',
		b.SalesTerritoryCountry as 'Country',
		b.SalesTerritoryRegion as 'Region'
from FactResellerSales  a
join DimSalesTerritory b
on a.SalesTerritoryKey = b.SalesTerritoryKey
group by b.SalesTerritoryGroup ,b.SalesTerritoryCountry , b.SalesTerritoryRegion 

union

select  sum(a.SalesAmount) as 'TotalSales',
		sum(a.TotalProductCost) as 'ProductCost',
		sum(a.SalesAmount - a.TotalProductCost) as 'Profit',
		sum(a.OrderQuantity) as 'Quantity',
		count(distinct a.SalesOrderNumber) as 'OrderNumbers',
		b.SalesTerritoryGroup as 'GroupName',
		b.SalesTerritoryCountry as 'Country',
		b.SalesTerritoryRegion as 'Region'
from FactInternetSales  a
join DimSalesTerritory b
on a.SalesTerritoryKey = b.SalesTerritoryKey
group by b.SalesTerritoryGroup ,b.SalesTerritoryCountry , b.SalesTerritoryRegion 

) R
group by R.GroupName , R.Country ,R.Region 
) K
order by Rank
 
--13. What are the sales by year by sales channels (internet, reseller & total)? 
	
select sum(a.SalesAmount) as TotalSales , YEAR(a.OrderDate)  as 'yearofOrder' , 'Reseller' as 'Channel'
from FactResellerSales a
group by YEAR(a.OrderDate) 

UNION

select sum(a.SalesAmount) as TotalSales , YEAR(a.OrderDate)  as 'year' , 'Internet' as 'Channel'
from FactInternetSales a
group by YEAR(a.OrderDate)

UNION

SELECT SUM(TotalSales) as TotalSales, TOTAL.yearofOrder as 'yearofOrder', 'Total' as 'Channel' 
FROM (select sum(a.SalesAmount) as TotalSales , YEAR(a.OrderDate)  as 'yearofOrder' , 'Reseller' as 'Channel'
from FactResellerSales a
group by YEAR(a.OrderDate) 

UNION

select sum(a.SalesAmount) as TotalSales , YEAR(a.OrderDate)  as 'year' , 'Internet' as 'Channel'
from FactInternetSales a
group by YEAR(a.OrderDate)) TOTAL
group by total.yearofOrder 
 

--14. What are the total sales by month (& year)? 
	select R.Year as 'Year',R.Month as 'Month',sum(R.TotalSales) as TotalSalest from
(
select 
year(a.OrderDate) as 'Year',
month(a.OrderDate) as 'Month',
sum(a.SalesAmount) as TotalSales from FactInternetSales as a
group by year(a.OrderDate),month(a.OrderDate)
Union
select 
year(a.OrderDate) as 'Year',
month(a.OrderDate) as 'Month',
sum(a.SalesAmount) as TotalSales from FactResellerSales as a
group by year(a.OrderDate),month(a.OrderDate)
)R
group by R.Month,R.Year
order by TotalSalest desc
 
--15. Please explain (briefly) the differences between SQL queries used to answer the same questions between AdventureWorksDW2017 & AdventureWorks2017
--•	AdventureWorksDW2017 has more of a segregated data so it is easy to iterate through all the tables to get the required results.
--•	AdventureWorksDW2017 is a single schema db so it has direct relations with all the related tables.
--•	AdventureWorksDW2017 is a better data source for analysis of data
--•	In DW there were different tables for internet and reseller whereas in OLTP we had to bifurcate on the basis of a flag (OnlineOrderFlag).
--•	We had Orderdatekey in DW which made it easy to get the year and month while quering for the same.
--•	It was easier to get the Total sales for all the available seller regardless of platform in OLTP as all the data was in one table. In DW union had to be done for Internet and Reseller.
--•	Getting occupation result was easier in DW as we had direct column with EnglishOccupation in the Table.
--•	Total sales by year was easier to get in the OLTP as in DW there were different tables with internet and reseller sales which resulted in multiple unions making it difficult to use ‘rollup’ as well.
--•	Getting Territory details as country, city, region was easier in DW as it was segregated in one table whereas OLTP had all the parameters in different table.

 
15. Please explain (briefly) the differences between SQL queries used to answer the same questions between AdventureWorksDW2017 & AdventureWorks2017
