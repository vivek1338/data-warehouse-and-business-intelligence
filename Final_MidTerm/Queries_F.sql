select dense_rank() over   (order by TotalDue desc) as 'Rank',  
TotalDue,
VendorName
from dimvendor a
join factpurchase b on a.VendorKey_SK = b.VendorKey_SK
group by VendorName;

------------------------------------------------------------------------------------------------------------------------

select dense_rank() over   (order by TotalDue desc) as 'Rank',  
TotalDue,
ProductCategoryName
from dimproduct a
join factpurchase b on b.ProductKey = a.ProductKey_NK
group by a.ProductCategoryName;

select dense_rank() over   (order by TotalDue desc) as 'Rank',  
TotalDue,
ProductSubCategoryName
from dimproduct a
join factpurchase b on b.ProductKey = a.ProductKey_NK
group by a.ProductSubCategoryName ;

select dense_rank() over   (order by TotalDue desc) as 'Rank',  
TotalDue,
a.ProductModelName
from dimproduct a
join factpurchase b on b.ProductKey = a.ProductKey_NK
group by a.ProductModelName ;

select dense_rank() over   (order by TotalDue desc) as 'Rank',  
TotalDue,
a.ProductName
from dimproduct a
join factpurchase b on b.ProductKey = a.ProductKey_NK
group by a.ProductName ;

------------------------------------------------------------------------------------------------------------------------

select fp.purchaseorderid,p.ProductKey_NK, lp.listprice from dimproduct p
join dimlistpricehistory lp on lp.productkey_sk = p.productkey_sk
join factpurchase fp on fp.productkey_sk = p.productkey_sk
order by p.productkey_nk, lp.listprice;

------------------------------------------------------------------------------------------------------------------------

select fp.purchaseorderid, p.ProductKey_NK, sc.costprice from dimproduct p
join dimcostpricehistory sc on sc.productkey_sk = p.productkey_sk
join factpurchase fp on fp.productkey_sk = p.productkey_sk
order by p.productkey_nk, sc.costprice;

------------------------------------------------------------------------------------------------------------------------

select VendorKey_SK, firstname, lastname,phone,emailaddress  from dimvendorcontact;