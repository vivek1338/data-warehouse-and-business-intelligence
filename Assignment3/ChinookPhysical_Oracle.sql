--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      ER_ChinookDW.DM1
--
-- Date Created : Thursday, January 30, 2020 00:11:04
-- Target DBMS : Oracle 11g
--

-- 
-- TABLE: DimAlbum 
--

CREATE TABLE DimAlbum(
    AlbumID       NUMBER(38, 0)     NOT NULL,
    Title         NVARCHAR2(160)    NOT NULL,
    ArtistName    NVARCHAR2(120),
    CONSTRAINT PK1_2_1_2 PRIMARY KEY (AlbumID)
)
;



-- 
-- TABLE: DimContact 
--

CREATE TABLE DimContact(
    ContactID      NUMBER(38, 0)    NOT NULL,
    Phone          NVARCHAR2(24),
    City           NVARCHAR2(24),
    emailAdress    NVARCHAR2(60),
    CONSTRAINT PK1_2_1 PRIMARY KEY (ContactID)
)
;



-- 
-- TABLE: DimCustomer 
--

CREATE TABLE DimCustomer(
    CustomerID     NUMBER(38, 0)    NOT NULL,
    FirstName      NVARCHAR2(40)    NOT NULL,
    Company        NVARCHAR2(80),
    LastName       NVARCHAR2(20)    NOT NULL,
    ContactID      NUMBER(38, 0)    NOT NULL,
    GeographyID    NUMBER(38, 0)    NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY (CustomerID)
)
;



-- 
-- TABLE: DimDate 
--

CREATE TABLE DimDate(
    DateKey                 NUMBER(38, 0)    NOT NULL,
    FullDateAlternateKey    DATE             NOT NULL,
    DayNumberOfWeek         NUMBER(3, 0)     NOT NULL,
    EnglishDayNameOfWeek    NVARCHAR2(10)    NOT NULL,
    SpanishDayNameOfWeek    NVARCHAR2(10)    NOT NULL,
    FrenchDayNameOfWeek     NVARCHAR2(10)    NOT NULL,
    DayNumberOfMonth        NUMBER(3, 0)     NOT NULL,
    DayNumberOfYear         SMALLINT         NOT NULL,
    WeekNumberOfYear        NUMBER(3, 0)     NOT NULL,
    EnglishMonthName        NVARCHAR2(10)    NOT NULL,
    SpanishMonthName        NVARCHAR2(10)    NOT NULL,
    FrenchMonthName         NVARCHAR2(10)    NOT NULL,
    MonthNumberOfYear       NUMBER(3, 0)     NOT NULL,
    CalendarQuarter         NUMBER(3, 0)     NOT NULL,
    CalendarYear            SMALLINT         NOT NULL,
    CalendarSemester        NUMBER(3, 0)     NOT NULL,
    FiscalQuarter           NUMBER(3, 0)     NOT NULL,
    FiscalYear              SMALLINT         NOT NULL,
    FiscalSemester          NUMBER(3, 0)     NOT NULL,
    CONSTRAINT PK_DimDate_DateKey PRIMARY KEY (DateKey)
)
;



-- 
-- TABLE: DimEmployee 
--

CREATE TABLE DimEmployee(
    EmployeeId     NUMBER(38, 0)    NOT NULL,
    BirthDate      TIMESTAMP(6),
    HireDate       TIMESTAMP(6),
    ReportTo       NUMBER(38, 0)    NOT NULL,
    FirstName      NVARCHAR2(20)    NOT NULL,
    Company        NVARCHAR2(30),
    LastName       NVARCHAR2(20)    NOT NULL,
    ContactID      NUMBER(38, 0)    NOT NULL,
    GeographyID    NUMBER(38, 0)    NOT NULL,
    CONSTRAINT PK1_1 PRIMARY KEY (EmployeeId)
)
;



-- 
-- TABLE: DimGeography 
--

CREATE TABLE DimGeography(
    GeographyID    NUMBER(38, 0)    NOT NULL,
    Address        NVARCHAR2(70)    NOT NULL,
    City           NVARCHAR2(40)    NOT NULL,
    State          NVARCHAR2(40),
    Country        NVARCHAR2(40)    NOT NULL,
    PostalCode     NVARCHAR2(10)    NOT NULL,
    CONSTRAINT PK1_2 PRIMARY KEY (GeographyID)
)
;



-- 
-- TABLE: DimPlaylist 
--

CREATE TABLE DimPlaylist(
    PlaylistID      NUMBER(38, 0)     NOT NULL,
    PlaylistName    NVARCHAR2(120),
    CONSTRAINT PK1_2_1_2_1 PRIMARY KEY (PlaylistID)
)
;



-- 
-- TABLE: DimTrack 
--

CREATE TABLE DimTrack(
    TrackID         NUMBER(38, 0)     NOT NULL,
    TrackName       NVARCHAR2(200)    NOT NULL,
    MediaName       NVARCHAR2(120),
    GenreName       NVARCHAR2(120),
    Composer        NVARCHAR2(220),
    MilliSeconds    NUMBER(38, 0)     NOT NULL,
    Bytes           NUMBER(38, 0),
    UnitPrice       NUMBER(10, 2)     NOT NULL,
    PlaylistID      NUMBER(38, 0),
    AlbumID         NUMBER(38, 0),
    CONSTRAINT PK1_2_1_1 PRIMARY KEY (TrackID)
)
;



-- 
-- TABLE: FactInvoice 
--

CREATE TABLE FactInvoice(
    InvoiceID      NUMBER(38, 0)    NOT NULL,
    GeographyID    NUMBER(38, 0)    NOT NULL,
    CustomerID     NUMBER(38, 0)    NOT NULL,
    EmployeeId     NUMBER(38, 0)    NOT NULL,
    UnitPrice      NUMBER(10, 2)    NOT NULL,
    Quantity       NUMBER(38, 0)    NOT NULL,
    TrackID        NUMBER(38, 0)    NOT NULL,
    DateKey        NUMBER(38, 0)    NOT NULL,
    CONSTRAINT PK8 PRIMARY KEY (InvoiceID)
)
;



-- 
-- TABLE: DimCustomer 
--

ALTER TABLE DimCustomer ADD CONSTRAINT RefDimContact34 
    FOREIGN KEY (ContactID)
    REFERENCES DimContact(ContactID)
;

ALTER TABLE DimCustomer ADD CONSTRAINT RefDimGeography37 
    FOREIGN KEY (GeographyID)
    REFERENCES DimGeography(GeographyID)
;


-- 
-- TABLE: DimEmployee 
--

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimEmployee1 
    FOREIGN KEY (ReportTo)
    REFERENCES DimEmployee(EmployeeId)
;

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimContact33 
    FOREIGN KEY (ContactID)
    REFERENCES DimContact(ContactID)
;

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimGeography38 
    FOREIGN KEY (GeographyID)
    REFERENCES DimGeography(GeographyID)
;


-- 
-- TABLE: DimTrack 
--

ALTER TABLE DimTrack ADD CONSTRAINT RefDimPlaylist19 
    FOREIGN KEY (PlaylistID)
    REFERENCES DimPlaylist(PlaylistID)
;

ALTER TABLE DimTrack ADD CONSTRAINT RefDimAlbum20 
    FOREIGN KEY (AlbumID)
    REFERENCES DimAlbum(AlbumID)
;


-- 
-- TABLE: FactInvoice 
--

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimGeography12 
    FOREIGN KEY (GeographyID)
    REFERENCES DimGeography(GeographyID)
;

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimTrack16 
    FOREIGN KEY (TrackID)
    REFERENCES DimTrack(TrackID)
;

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimDate23 
    FOREIGN KEY (DateKey)
    REFERENCES DimDate(DateKey)
;

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimCustomer31 
    FOREIGN KEY (CustomerID)
    REFERENCES DimCustomer(CustomerID)
;

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimEmployee32 
    FOREIGN KEY (EmployeeId)
    REFERENCES DimEmployee(EmployeeId)
;


