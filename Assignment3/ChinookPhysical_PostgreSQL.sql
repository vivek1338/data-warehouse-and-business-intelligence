--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      ER_ChinookDW.DM1
--
-- Date Created : Thursday, January 30, 2020 00:11:21
-- Target DBMS : PostgreSQL 9.x
--

-- 
-- TABLE: "DimAlbum" 
--

CREATE TABLE "DimAlbum"(
    "AlbumID"     int4            NOT NULL,
    "Title"       varchar(160)    NOT NULL,
    "ArtistName"  varchar(120),
    CONSTRAINT "PK1_2_1_2" PRIMARY KEY ("AlbumID")
)
;



-- 
-- TABLE: "DimContact" 
--

CREATE TABLE "DimContact"(
    "ContactID"    int4           NOT NULL,
    "Phone"        varchar(24),
    "City"         varchar(24),
    "emailAdress"  varchar(60),
    CONSTRAINT "PK1_2_1" PRIMARY KEY ("ContactID")
)
;



-- 
-- TABLE: "DimCustomer" 
--

CREATE TABLE "DimCustomer"(
    "CustomerID"   int4           NOT NULL,
    "FirstName"    varchar(40)    NOT NULL,
    "Company"      varchar(80),
    "LastName"     varchar(20)    NOT NULL,
    "ContactID"    int4           NOT NULL,
    "GeographyID"  int4           NOT NULL,
    CONSTRAINT "PK1" PRIMARY KEY ("CustomerID")
)
;



-- 
-- TABLE: "DimDate" 
--

CREATE TABLE "DimDate"(
    "DateKey"               int4           NOT NULL,
    "FullDateAlternateKey"  date           NOT NULL,
    "DayNumberOfWeek"       int2           NOT NULL,
    "EnglishDayNameOfWeek"  varchar(10)    NOT NULL,
    "SpanishDayNameOfWeek"  varchar(10)    NOT NULL,
    "FrenchDayNameOfWeek"   varchar(10)    NOT NULL,
    "DayNumberOfMonth"      int2           NOT NULL,
    "DayNumberOfYear"       int2           NOT NULL,
    "WeekNumberOfYear"      int2           NOT NULL,
    "EnglishMonthName"      varchar(10)    NOT NULL,
    "SpanishMonthName"      varchar(10)    NOT NULL,
    "FrenchMonthName"       varchar(10)    NOT NULL,
    "MonthNumberOfYear"     int2           NOT NULL,
    "CalendarQuarter"       int2           NOT NULL,
    "CalendarYear"          int2           NOT NULL,
    "CalendarSemester"      int2           NOT NULL,
    "FiscalQuarter"         int2           NOT NULL,
    "FiscalYear"            int2           NOT NULL,
    "FiscalSemester"        int2           NOT NULL,
    CONSTRAINT "PK_DimDate_DateKey" PRIMARY KEY ("DateKey")
)
;



-- 
-- TABLE: "DimEmployee" 
--

CREATE TABLE "DimEmployee"(
    "EmployeeId"   int4           NOT NULL,
    "BirthDate"    timestamp,
    "HireDate"     timestamp,
    "ReportTo"     int4           NOT NULL,
    "FirstName"    varchar(20)    NOT NULL,
    "Company"      varchar(30),
    "LastName"     varchar(20)    NOT NULL,
    "ContactID"    int4           NOT NULL,
    "GeographyID"  int4           NOT NULL,
    CONSTRAINT "PK1_1" PRIMARY KEY ("EmployeeId")
)
;



-- 
-- TABLE: "DimGeography" 
--

CREATE TABLE "DimGeography"(
    "GeographyID"  int4           NOT NULL,
    "Address"      varchar(70)    NOT NULL,
    "City"         varchar(40)    NOT NULL,
    "State"        varchar(40),
    "Country"      varchar(40)    NOT NULL,
    "PostalCode"   varchar(10)    NOT NULL,
    CONSTRAINT "PK1_2" PRIMARY KEY ("GeographyID")
)
;



-- 
-- TABLE: "DimPlaylist" 
--

CREATE TABLE "DimPlaylist"(
    "PlaylistID"    int4            NOT NULL,
    "PlaylistName"  varchar(120),
    CONSTRAINT "PK1_2_1_2_1" PRIMARY KEY ("PlaylistID")
)
;



-- 
-- TABLE: "DimTrack" 
--

CREATE TABLE "DimTrack"(
    "TrackID"       int4              NOT NULL,
    "TrackName"     varchar(200)      NOT NULL,
    "MediaName"     varchar(120),
    "GenreName"     varchar(120),
    "Composer"      varchar(220),
    "MilliSeconds"  int4              NOT NULL,
    "Bytes"         int4,
    "UnitPrice"     numeric(10, 2)    NOT NULL,
    "PlaylistID"    int4,
    "AlbumID"       int4,
    CONSTRAINT "PK1_2_1_1" PRIMARY KEY ("TrackID")
)
;



-- 
-- TABLE: "FactInvoice" 
--

CREATE TABLE "FactInvoice"(
    "InvoiceID"    int4              NOT NULL,
    "GeographyID"  int4              NOT NULL,
    "CustomerID"   int4              NOT NULL,
    "EmployeeId"   int4              NOT NULL,
    "UnitPrice"    numeric(10, 2)    NOT NULL,
    "Quantity"     int4              NOT NULL,
    "TrackID"      int4              NOT NULL,
    "DateKey"      int4              NOT NULL,
    CONSTRAINT "PK8" PRIMARY KEY ("InvoiceID")
)
;



-- 
-- TABLE: "DimCustomer" 
--

ALTER TABLE "DimCustomer" ADD CONSTRAINT "RefDimContact34" 
    FOREIGN KEY ("ContactID")
    REFERENCES "DimContact"("ContactID")
;

ALTER TABLE "DimCustomer" ADD CONSTRAINT "RefDimGeography37" 
    FOREIGN KEY ("GeographyID")
    REFERENCES "DimGeography"("GeographyID")
;


-- 
-- TABLE: "DimEmployee" 
--

ALTER TABLE "DimEmployee" ADD CONSTRAINT "RefDimEmployee1" 
    FOREIGN KEY ("ReportTo")
    REFERENCES "DimEmployee"("EmployeeId")
;

ALTER TABLE "DimEmployee" ADD CONSTRAINT "RefDimContact33" 
    FOREIGN KEY ("ContactID")
    REFERENCES "DimContact"("ContactID")
;

ALTER TABLE "DimEmployee" ADD CONSTRAINT "RefDimGeography38" 
    FOREIGN KEY ("GeographyID")
    REFERENCES "DimGeography"("GeographyID")
;


-- 
-- TABLE: "DimTrack" 
--

ALTER TABLE "DimTrack" ADD CONSTRAINT "RefDimPlaylist19" 
    FOREIGN KEY ("PlaylistID")
    REFERENCES "DimPlaylist"("PlaylistID")
;

ALTER TABLE "DimTrack" ADD CONSTRAINT "RefDimAlbum20" 
    FOREIGN KEY ("AlbumID")
    REFERENCES "DimAlbum"("AlbumID")
;


-- 
-- TABLE: "FactInvoice" 
--

ALTER TABLE "FactInvoice" ADD CONSTRAINT "RefDimGeography12" 
    FOREIGN KEY ("GeographyID")
    REFERENCES "DimGeography"("GeographyID")
;

ALTER TABLE "FactInvoice" ADD CONSTRAINT "RefDimTrack16" 
    FOREIGN KEY ("TrackID")
    REFERENCES "DimTrack"("TrackID")
;

ALTER TABLE "FactInvoice" ADD CONSTRAINT "RefDimDate23" 
    FOREIGN KEY ("DateKey")
    REFERENCES "DimDate"("DateKey")
;

ALTER TABLE "FactInvoice" ADD CONSTRAINT "RefDimCustomer31" 
    FOREIGN KEY ("CustomerID")
    REFERENCES "DimCustomer"("CustomerID")
;

ALTER TABLE "FactInvoice" ADD CONSTRAINT "RefDimEmployee32" 
    FOREIGN KEY ("EmployeeId")
    REFERENCES "DimEmployee"("EmployeeId")
;


