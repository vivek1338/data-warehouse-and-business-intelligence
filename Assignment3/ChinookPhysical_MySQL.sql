--
-- ER/Studio Data Architect SQL Code Generation
-- Project :      ER_ChinookDW.DM1
--
-- Date Created : Thursday, January 30, 2020 00:10:14
-- Target DBMS : MySQL 5.x
--

-- 
-- TABLE: DimAlbum 
--

CREATE TABLE DimAlbum(
    AlbumID       INT                      NOT NULL,
    Title         NATIONAL VARCHAR(160)    NOT NULL,
    ArtistName    NATIONAL VARCHAR(120),
    PRIMARY KEY (AlbumID)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimContact 
--

CREATE TABLE DimContact(
    ContactID      INT                     NOT NULL,
    Phone          NATIONAL VARCHAR(24),
    City           NATIONAL VARCHAR(24),
    emailAdress    NATIONAL VARCHAR(60),
    PRIMARY KEY (ContactID)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimCustomer 
--

CREATE TABLE DimCustomer(
    CustomerID     INT                     NOT NULL,
    FirstName      NATIONAL VARCHAR(40)    NOT NULL,
    Company        NATIONAL VARCHAR(80),
    LastName       NATIONAL VARCHAR(20)    NOT NULL,
    ContactID      INT                     NOT NULL,
    GeographyID    INT                     NOT NULL,
    PRIMARY KEY (CustomerID)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimDate 
--

CREATE TABLE DimDate(
    DateKey                 INT                     NOT NULL,
    FullDateAlternateKey    DATE                    NOT NULL,
    DayNumberOfWeek         TINYINT                 NOT NULL,
    EnglishDayNameOfWeek    NATIONAL VARCHAR(10)    NOT NULL,
    SpanishDayNameOfWeek    NATIONAL VARCHAR(10)    NOT NULL,
    FrenchDayNameOfWeek     NATIONAL VARCHAR(10)    NOT NULL,
    DayNumberOfMonth        TINYINT                 NOT NULL,
    DayNumberOfYear         SMALLINT                NOT NULL,
    WeekNumberOfYear        TINYINT                 NOT NULL,
    EnglishMonthName        NATIONAL VARCHAR(10)    NOT NULL,
    SpanishMonthName        NATIONAL VARCHAR(10)    NOT NULL,
    FrenchMonthName         NATIONAL VARCHAR(10)    NOT NULL,
    MonthNumberOfYear       TINYINT                 NOT NULL,
    CalendarQuarter         TINYINT                 NOT NULL,
    CalendarYear            SMALLINT                NOT NULL,
    CalendarSemester        TINYINT                 NOT NULL,
    FiscalQuarter           TINYINT                 NOT NULL,
    FiscalYear              SMALLINT                NOT NULL,
    FiscalSemester          TINYINT                 NOT NULL,
    PRIMARY KEY (DateKey)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimEmployee 
--

CREATE TABLE DimEmployee(
    EmployeeId     INT                     NOT NULL,
    BirthDate      DATETIME,
    HireDate       DATETIME,
    ReportTo       INT                     NOT NULL,
    FirstName      NATIONAL VARCHAR(20)    NOT NULL,
    Company        NATIONAL VARCHAR(30),
    LastName       NATIONAL VARCHAR(20)    NOT NULL,
    ContactID      INT                     NOT NULL,
    GeographyID    INT                     NOT NULL,
    PRIMARY KEY (EmployeeId)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimGeography 
--

CREATE TABLE DimGeography(
    GeographyID    INT                     NOT NULL,
    Address        NATIONAL VARCHAR(70)    NOT NULL,
    City           NATIONAL VARCHAR(40)    NOT NULL,
    State          NATIONAL VARCHAR(40),
    Country        NATIONAL VARCHAR(40)    NOT NULL,
    PostalCode     NATIONAL VARCHAR(10)    NOT NULL,
    PRIMARY KEY (GeographyID)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimPlaylist 
--

CREATE TABLE DimPlaylist(
    PlaylistID      INT                      NOT NULL,
    PlaylistName    NATIONAL VARCHAR(120),
    PRIMARY KEY (PlaylistID)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimTrack 
--

CREATE TABLE DimTrack(
    TrackID         INT                      NOT NULL,
    TrackName       NATIONAL VARCHAR(200)    NOT NULL,
    MediaName       NATIONAL VARCHAR(120),
    GenreName       NATIONAL VARCHAR(120),
    Composer        NATIONAL VARCHAR(220),
    MilliSeconds    INT                      NOT NULL,
    Bytes           INT,
    UnitPrice       DECIMAL(10, 2)           NOT NULL,
    PlaylistID      INT,
    AlbumID         INT,
    PRIMARY KEY (TrackID)
)ENGINE=MYISAM
;



-- 
-- TABLE: FactInvoice 
--

CREATE TABLE FactInvoice(
    InvoiceID      INT               NOT NULL,
    GeographyID    INT               NOT NULL,
    CustomerID     INT               NOT NULL,
    EmployeeId     INT               NOT NULL,
    UnitPrice      DECIMAL(10, 2)    NOT NULL,
    Quantity       INT               NOT NULL,
    TrackID        INT               NOT NULL,
    DateKey        INT               NOT NULL,
    PRIMARY KEY (InvoiceID)
)ENGINE=MYISAM
;



-- 
-- TABLE: DimCustomer 
--

ALTER TABLE DimCustomer ADD CONSTRAINT RefDimContact34 
    FOREIGN KEY (ContactID)
    REFERENCES DimContact(ContactID)
;

ALTER TABLE DimCustomer ADD CONSTRAINT RefDimGeography37 
    FOREIGN KEY (GeographyID)
    REFERENCES DimGeography(GeographyID)
;


-- 
-- TABLE: DimEmployee 
--

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimEmployee1 
    FOREIGN KEY (ReportTo)
    REFERENCES DimEmployee(EmployeeId)
;

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimContact33 
    FOREIGN KEY (ContactID)
    REFERENCES DimContact(ContactID)
;

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimGeography38 
    FOREIGN KEY (GeographyID)
    REFERENCES DimGeography(GeographyID)
;


-- 
-- TABLE: DimTrack 
--

ALTER TABLE DimTrack ADD CONSTRAINT RefDimPlaylist19 
    FOREIGN KEY (PlaylistID)
    REFERENCES DimPlaylist(PlaylistID)
;

ALTER TABLE DimTrack ADD CONSTRAINT RefDimAlbum20 
    FOREIGN KEY (AlbumID)
    REFERENCES DimAlbum(AlbumID)
;


-- 
-- TABLE: FactInvoice 
--

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimGeography12 
    FOREIGN KEY (GeographyID)
    REFERENCES DimGeography(GeographyID)
;

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimTrack16 
    FOREIGN KEY (TrackID)
    REFERENCES DimTrack(TrackID)
;

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimDate23 
    FOREIGN KEY (DateKey)
    REFERENCES DimDate(DateKey)
;

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimCustomer31 
    FOREIGN KEY (CustomerID)
    REFERENCES DimCustomer(CustomerID)
;

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimEmployee32 
    FOREIGN KEY (EmployeeId)
    REFERENCES DimEmployee(EmployeeId)
;


