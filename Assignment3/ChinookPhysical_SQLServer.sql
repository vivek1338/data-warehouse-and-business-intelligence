/*
 * ER/Studio Data Architect SQL Code Generation
 * Project :      ER_ChinookDW.DM1
 *
 * Date Created : Thursday, January 30, 2020 00:11:39
 * Target DBMS : Microsoft SQL Server 2017
 */

/* 
 * TABLE: DimAlbum 
 */

CREATE TABLE DimAlbum(
    AlbumID       int              NOT NULL,
    ArtistName    nvarchar(120)    NULL,
    AlbumTitle    nvarchar(160)    NOT NULL,
    CONSTRAINT PK1_2_1_2 PRIMARY KEY NONCLUSTERED (AlbumID)
)
go



IF OBJECT_ID('DimAlbum') IS NOT NULL
    PRINT '<<< CREATED TABLE DimAlbum >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimAlbum >>>'
go

/* 
 * TABLE: DimContact 
 */

CREATE TABLE DimContact(
    ContactID      int             NOT NULL,
    Phone          nvarchar(24)    NULL,
    City           nvarchar(24)    NULL,
    EmailAdress    nvarchar(60)    NULL,
    CONSTRAINT PK1_2_1 PRIMARY KEY NONCLUSTERED (ContactID)
)
go



IF OBJECT_ID('DimContact') IS NOT NULL
    PRINT '<<< CREATED TABLE DimContact >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimContact >>>'
go

/* 
 * TABLE: DimCustomer 
 */

CREATE TABLE DimCustomer(
    CustomerID     int             NOT NULL,
    FirstName      nvarchar(40)    NOT NULL,
    LastName       nvarchar(20)    NOT NULL,
    CompanyName    nvarchar(80)    NULL,
    ContactID      int             NOT NULL,
    GeographyID    int             NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY NONCLUSTERED (CustomerID)
)
go



IF OBJECT_ID('DimCustomer') IS NOT NULL
    PRINT '<<< CREATED TABLE DimCustomer >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimCustomer >>>'
go

/* 
 * TABLE: DimDate 
 */

CREATE TABLE DimDate(
    DateKey                 int             NOT NULL,
    FullDateAlternateKey    date            NOT NULL,
    DayNumberOfWeek         tinyint         NOT NULL,
    EnglishDayNameOfWeek    nvarchar(10)    NOT NULL,
    SpanishDayNameOfWeek    nvarchar(10)    NOT NULL,
    FrenchDayNameOfWeek     nvarchar(10)    NOT NULL,
    DayNumberOfMonth        tinyint         NOT NULL,
    DayNumberOfYear         smallint        NOT NULL,
    WeekNumberOfYear        tinyint         NOT NULL,
    EnglishMonthName        nvarchar(10)    NOT NULL,
    SpanishMonthName        nvarchar(10)    NOT NULL,
    FrenchMonthName         nvarchar(10)    NOT NULL,
    MonthNumberOfYear       tinyint         NOT NULL,
    CalendarQuarter         tinyint         NOT NULL,
    CalendarYear            smallint        NOT NULL,
    CalendarSemester        tinyint         NOT NULL,
    FiscalQuarter           tinyint         NOT NULL,
    FiscalYear              smallint        NOT NULL,
    FiscalSemester          tinyint         NOT NULL,
    CONSTRAINT PK_DimDate_DateKey PRIMARY KEY NONCLUSTERED (DateKey)
)
go



IF OBJECT_ID('DimDate') IS NOT NULL
    PRINT '<<< CREATED TABLE DimDate >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimDate >>>'
go

/* 
 * TABLE: DimEmployee 
 */

CREATE TABLE DimEmployee(
    EmployeeId     int             NOT NULL,
    BirthDate      datetime        NULL,
    HiringDate     datetime        NULL,
    ReportTo       int             NOT NULL,
    FirstName      nvarchar(20)    NOT NULL,
    Company        nvarchar(30)    NULL,
    LastName       nvarchar(20)    NOT NULL,
    ContactID      int             NOT NULL,
    GeographyID    int             NOT NULL,
    CONSTRAINT PK1_1 PRIMARY KEY NONCLUSTERED (EmployeeId)
)
go



IF OBJECT_ID('DimEmployee') IS NOT NULL
    PRINT '<<< CREATED TABLE DimEmployee >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimEmployee >>>'
go

/* 
 * TABLE: DimGeography 
 */

CREATE TABLE DimGeography(
    GeographyID    int             NOT NULL,
    Address        nvarchar(70)    NOT NULL,
    City           nvarchar(40)    NOT NULL,
    State          nvarchar(40)    NULL,
    Country        nvarchar(40)    NOT NULL,
    PostalCode     nvarchar(10)    NOT NULL,
    CONSTRAINT PK1_2 PRIMARY KEY NONCLUSTERED (GeographyID)
)
go



IF OBJECT_ID('DimGeography') IS NOT NULL
    PRINT '<<< CREATED TABLE DimGeography >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimGeography >>>'
go

/* 
 * TABLE: DimPlaylist 
 */

CREATE TABLE DimPlaylist(
    PlaylistID      int              NOT NULL,
    PlaylistName    nvarchar(120)    NULL,
    CONSTRAINT PK1_2_1_2_1 PRIMARY KEY NONCLUSTERED (PlaylistID)
)
go



IF OBJECT_ID('DimPlaylist') IS NOT NULL
    PRINT '<<< CREATED TABLE DimPlaylist >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimPlaylist >>>'
go

/* 
 * TABLE: DimTrack 
 */

CREATE TABLE DimTrack(
    TrackID         int               NOT NULL,
    TrackName       nvarchar(200)     NOT NULL,
    MediaName       nvarchar(120)     NULL,
    Bytes           int               NULL,
    UnitPrice       numeric(10, 2)    NOT NULL,
    GenreName       nvarchar(120)     NULL,
    MilliSeconds    int               NOT NULL,
    ComposerName    nvarchar(220)     NULL,
    PlaylistID      int               NULL,
    AlbumID         int               NULL,
    CONSTRAINT PK1_2_1_1 PRIMARY KEY NONCLUSTERED (TrackID)
)
go



IF OBJECT_ID('DimTrack') IS NOT NULL
    PRINT '<<< CREATED TABLE DimTrack >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimTrack >>>'
go

/* 
 * TABLE: FactInvoice 
 */

CREATE TABLE FactInvoice(
    InvoiceID      int               NOT NULL,
    GeographyID    int               NOT NULL,
    CustomerID     int               NOT NULL,
    EmployeeId     int               NOT NULL,
    UnitPrice      numeric(10, 2)    NOT NULL,
    Quantity       int               NOT NULL,
    TrackID        int               NOT NULL,
    DateKey        int               NOT NULL,
    CONSTRAINT PK8 PRIMARY KEY NONCLUSTERED (InvoiceID)
)
go



IF OBJECT_ID('FactInvoice') IS NOT NULL
    PRINT '<<< CREATED TABLE FactInvoice >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE FactInvoice >>>'
go

/* 
 * TABLE: DimCustomer 
 */

ALTER TABLE DimCustomer ADD CONSTRAINT RefDimContact34 
    FOREIGN KEY (ContactID)
    REFERENCES DimContact(ContactID)
go

ALTER TABLE DimCustomer ADD CONSTRAINT RefDimGeography37 
    FOREIGN KEY (GeographyID)
    REFERENCES DimGeography(GeographyID)
go


/* 
 * TABLE: DimEmployee 
 */

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimEmployee1 
    FOREIGN KEY (ReportTo)
    REFERENCES DimEmployee(EmployeeId)
go

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimContact33 
    FOREIGN KEY (ContactID)
    REFERENCES DimContact(ContactID)
go

ALTER TABLE DimEmployee ADD CONSTRAINT RefDimGeography38 
    FOREIGN KEY (GeographyID)
    REFERENCES DimGeography(GeographyID)
go


/* 
 * TABLE: DimTrack 
 */

ALTER TABLE DimTrack ADD CONSTRAINT RefDimPlaylist19 
    FOREIGN KEY (PlaylistID)
    REFERENCES DimPlaylist(PlaylistID)
go

ALTER TABLE DimTrack ADD CONSTRAINT RefDimAlbum20 
    FOREIGN KEY (AlbumID)
    REFERENCES DimAlbum(AlbumID)
go


/* 
 * TABLE: FactInvoice 
 */

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimGeography12 
    FOREIGN KEY (GeographyID)
    REFERENCES DimGeography(GeographyID)
go

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimTrack16 
    FOREIGN KEY (TrackID)
    REFERENCES DimTrack(TrackID)
go

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimDate23 
    FOREIGN KEY (DateKey)
    REFERENCES DimDate(DateKey)
go

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimCustomer31 
    FOREIGN KEY (CustomerID)
    REFERENCES DimCustomer(CustomerID)
go

ALTER TABLE FactInvoice ADD CONSTRAINT RefDimEmployee32 
    FOREIGN KEY (EmployeeId)
    REFERENCES DimEmployee(EmployeeId)
go


