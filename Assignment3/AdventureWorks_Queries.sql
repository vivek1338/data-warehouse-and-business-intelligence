﻿--Queries (SQL queries): 
--Answer using existing AdventureWorks2017, not new schema 
--o Ranked order of Vendors by purchase amount $ 

select 
rank() over   
    (order by sum(a.OrderQty*a.UnitPrice) desc) as Rank,  
c.Name, sum(a.OrderQty*a.UnitPrice) as 'TotalAmount' from [Purchasing].[PurchaseOrderDetail] a
join [Purchasing].[PurchaseOrderHeader] b on a.PurchaseOrderID = b.PurchaseOrderID
join [Purchasing].[Vendor] c on b.VendorID = c.BusinessEntityID
group by c.Name

--o Ranked order of products purchased by amount $ 

--▪ By category 

select 
rank() over   
(order by sum(a.OrderQty*a.UnitPrice) desc) as Rank,  
e.Name, sum(a.OrderQty*a.UnitPrice) as 'TotalAmount' from [Purchasing].[PurchaseOrderDetail] a
join [Purchasing].[PurchaseOrderHeader] b on a.PurchaseOrderID = b.PurchaseOrderID
join [Production].[Product] c on a.ProductID = c.ProductID
join [Production].[ProductSubcategory] d on c.ProductSubcategoryID = d.ProductSubcategoryID
join [Production].[ProductCategory] e on d.ProductCategoryID = e.ProductCategoryID
group by e.Name

--▪ By subcategory 

select 
rank() over   
(order by sum(a.OrderQty*a.UnitPrice) desc) as Rank,  
d.Name, sum(a.OrderQty*a.UnitPrice) as 'TotalAmount' from [Purchasing].[PurchaseOrderDetail] a
join [Purchasing].[PurchaseOrderHeader] b on a.PurchaseOrderID = b.PurchaseOrderID
join [Production].[Product] c on a.ProductID = c.ProductID
join [Production].[ProductSubcategory] d on c.ProductSubcategoryID = d.ProductSubcategoryID
group by d.Name

--▪ By product model (top 20) 

select top 20
	rank() over   
	    (order by sum(a.OrderQty*a.UnitPrice) desc) as Rank,  
	d.Name, sum(a.OrderQty*a.UnitPrice) as 'TotalAmount' from [Purchasing].[PurchaseOrderDetail] a
	join [Purchasing].[PurchaseOrderHeader] b on a.PurchaseOrderID = b.PurchaseOrderID
	join [Production].[Product] c on a.ProductID = c.ProductID
	join [Production].[ProductModel] d on c.ProductModelID = d.ProductModelID
group by d.Name

--▪ By product (top 20) 

select top 20
		rank() over   
	      (order by sum(a.OrderQty*a.UnitPrice) desc) as Rank,  
	c.Name, sum(a.OrderQty*a.UnitPrice) as 'TotalAmount' from      [Purchasing].[PurchaseOrderDetail] a
	join [Purchasing].[PurchaseOrderHeader] b on a.PurchaseOrderID = b.PurchaseOrderID
	join [Production].[Product] c on a.ProductID = c.ProductID
	group by c.Name

--o List of employees who purchased products with phone, email & address 

select 
e.LastName+', '+e.FirstName as FullName,
e.PhoneNumber,
e.EmailAddress,
e.AddressLine1,
e.AddressLine2 from [Purchasing].[PurchaseOrderHeader] a
join [HumanResources].[Employee] d on a.EmployeeID = d.BusinessEntityID
join [HumanResources].[vEmployee] e on d.BusinessEntityID = e.BusinessEntityID
group by a.EmployeeID,e.LastName,e.FirstName,e.PhoneNumber,e.EmailAddress,e.AddressLine1,e.AddressLine2

--o List of employees who purchased products with pay rate & raises (SCD) 

select distinct concat(c.LastName, ', ', c.FirstName) as 'Employee Name',
                k.Rate,
                k.RateChangeDate,
                sum(totaldue) as 'Total Amount'
FROM [Purchasing].[PurchaseOrderDetail] as a
join [Purchasing].[PurchaseOrderHeader] as b on b.PurchaseOrderID=a.PurchaseOrderID
join [Person].[Person] as c on c.BusinessEntityID= b.EmployeeID
join [HumanResources].[Employee] as e on c.BusinessEntityID=e.BusinessEntityID
join [Person].[PersonPhone] as f on f.BusinessEntityID=c.BusinessEntityID
join [Person].[EmailAddress] as g on g.BusinessEntityID=c.BusinessEntityID
join [Person].[BusinessEntity] as h on c.BusinessEntityID=h.BusinessEntityID
join [Person].[BusinessEntityAddress] as i on i.BusinessEntityID=h.BusinessEntityID
join [Person].[Address] as j on i.AddressID=j.AddressID
join [HumanResources].[EmployeePayHistory] as k on e.BusinessEntityID=k.BusinessEntityID
group by concat(c.LastName, ', ', c.FirstName),
         k.Rate,
         k.RateChangeDate
order by [Total Amount] desc

--o List of purchasing vendor contacts with vendor name, phone, email & address 

select b.Name as VendorName,
a.PhoneNumber,
a.EmailAddress,
b.AddressLine1,
b.AddressLine2 
from [Purchasing].[vVendorWithContacts] a
join [Purchasing].[vVendorWithAddresses] b on a.BusinessEntityID = b.BusinessEntityID
group by b.Name,
a.PhoneNumber,
a.EmailAddress,
b.AddressLine1,
b.AddressLine2 


--o List of product prices by product order by product and SCD effective ascending

select a.Name as 'Product Name',
       b.StartDate,
       b.ModifiedDate,
       b.EndDate,
       b.ListPrice
from Production.Product as a
join Production.ProductListPriceHistory as b on b.ProductID = a.ProductID
group by a.Name,
         b.StartDate,
         b.ModifiedDate,
         b.EndDate,
         b.ListPrice
order by a.Name

--o List of standard costs by product order by product and SCD effective ascending

select p.Name as 'Product Name',
       pch.StartDate,
       pch.ModifiedDate,
       pch.EndDate,
       pch.StandardCost
from Production.Product as p
join Production.ProductCostHistory as pch on pch.ProductID = p.ProductID
group by p.Name,
         pch.StartDate,
         pch.ModifiedDate,
         pch.EndDate,
         pch.StandardCost
order by p.Name
